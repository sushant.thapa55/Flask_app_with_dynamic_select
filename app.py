from dataclasses import dataclass
from turtle import distance
from urllib import response
from flask import Flask,render_template,redirect,request
import json
import requests
import pickle
import pandas as pd


movies_dict = pickle.load(open('movie_dict.pkl','rb'))
movies = pd.DataFrame(movies_dict)
# print(type(movies))

similarity = pickle.load(open('similarity.pkl','rb'))

app = Flask(__name__)

def recommend(movie):
    movie_index = movies[movies['title']== movie].index[0]
    distances = similarity[movie_index]
    movie_list = sorted(list(enumerate(distances)), reverse=True,key=lambda x: x[1])[1:6]
    recommended_movies = []
    recommended_movies_poster=[]
    for i in movie_list:
        movie_id=movies.iloc[i[0]].movie_id
        recommended_movies.append(movies.iloc[i[0]].title)
        recommended_movies_poster.append(fetch_poster(movie_id))
    return recommended_movies,recommended_movies_poster


@app.route("/",methods= ['GET', 'POST'])
def hello_world():
    selected_type=request.form.get('select_type')
    return render_template('index.html',data=get_movies())


@app.route('/output.html',methods=['GET','POST'])
def test():

    selected_type=request.form.get('select_type')
    select_movie = request.form.get('categories')

    data,img_data=recommend(select_movie)
   

    return render_template('output.html',data=data,img=img_data)
   
def get_movies():
    title=movies['title'].values
    # print(type(title))
    # print(title)
    movie_title_list = title.tolist()
    return movie_title_list

def fetch_poster(movie_id):
    response = requests.get('https://api.themoviedb.org/3/movie/{}?api_key=13b296cb7e0e06ec938d0f9be39a66a9'.format(movie_id))
    data= response.json()
    # print(data)
    return "https://image.tmdb.org/t/p/w500/"+ data['poster_path']





# listmovie=recommend('Batman')
# print(listmovie)